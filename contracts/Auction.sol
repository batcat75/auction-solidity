pragma solidity ^0.4.8;

contract AuctionFactory {
    address[] public deployedAuctions;
    
    function createAuction(uint _startBid, uint _startTime, uint _endTime, string _description, string _ipfsHash) public {
        address newAuction = new Auction(msg.sender, _startBid, _startTime, _endTime, _description, _ipfsHash);
        deployedAuctions.push(newAuction);
    }
    
    function getDeployedAuction() public view returns (address[]) {
        return deployedAuctions;
    }
}

contract Auction {
    // static
    address public owner;
    uint public startBid;
    uint public startTime;
    uint public endTime;
    string public ipfsHash;
    string public description;

    // state
    bool public canceled;
    uint public highestBindingBid;
    address public highestBidder;
    mapping(address => uint256) public fundsByBidder;
    bool ownerHasWithdrawn;

    event LogBid(address bidder, uint bid, address highestBidder, uint highestBindingBid);
    event LogWithdrawal(address withdrawer, address withdrawalAccount, uint amount);
    event LogCanceled();

    constructor(address _owner, uint _startBid, uint _startTime, uint _endTime, string _description, string _ipfsHash) public {
        require (_startTime < _endTime);
        require (_endTime > now);
        require (_owner != address(0)) ;

        owner = _owner;
        startBid = _startBid;
        startTime = _startTime;
        endTime = _endTime;
        description = _description;
        ipfsHash = _ipfsHash;
    }

    function getHighestBid()
        public
        view
        returns (uint)
    {
        return fundsByBidder[highestBidder];
    }

    function placeBid()
        public
        payable
        onlyAfterStart
        onlyBeforeEnd
        onlyNotCanceled
        onlyNotOwner
        returns (bool success)
    {
        // reject payments of 0 ETH
        require(msg.value > 0);

        // calculate the user's total bid based on the current amount they've sent to the contract
        // plus whatever has been sent with this transaction
        uint newBid = fundsByBidder[msg.sender] + msg.value;
        
        // new bid should more than start bid
        require(newBid >= startBid);

        // new bid should more than highest binding bid
        require(newBid > highestBindingBid);

        fundsByBidder[msg.sender] = newBid;
        if (msg.sender != highestBidder) {
            highestBidder = msg.sender;
        }
        highestBindingBid = newBid;

        emit LogBid(msg.sender, newBid, highestBidder, highestBindingBid);
        return true;
    }

    function min(uint a, uint b)
        private
        pure
        returns (uint)
    {
        if (a < b) return a;
        return b;
    }

    function cancelAuction()
        public
        onlyOwner
        onlyBeforeEnd
        onlyNotCanceled
        returns (bool success)
    {
        canceled = true;
        emit LogCanceled();
        return true;
    }

    function withdraw()
        public
        onlyEndedOrCanceled
        returns (bool success)
    {
        // The winner doesn't have fund for withdraw
        require(msg.sender != highestBidder);
        
        address withdrawalAccount;
        uint withdrawalAmount;

        if (canceled) {
            // if the auction was canceled, everyone should simply be allowed to withdraw their funds
            withdrawalAccount = msg.sender;
            withdrawalAmount = fundsByBidder[withdrawalAccount];

        } else {
            // the auction finished without being canceled

            if (msg.sender == owner) {
                // Owner cannot withdraw twice
                require(!ownerHasWithdrawn);
                
                // the auction's owner should be allowed to withdraw the highestBindingBid
                withdrawalAccount = highestBidder;
                withdrawalAmount = highestBindingBid;
                ownerHasWithdrawn = true;

            } else {
                // anyone who participated but did not win the auction should be allowed to withdraw
                // the full amount of their funds
                withdrawalAccount = msg.sender;
                withdrawalAmount = fundsByBidder[withdrawalAccount];
            }
        }

        require(withdrawalAmount > 0);

        fundsByBidder[withdrawalAccount] -= withdrawalAmount;

        // send the funds
        if (!msg.sender.send(withdrawalAmount)) revert();

        emit LogWithdrawal(msg.sender, withdrawalAccount, withdrawalAmount);

        return true;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    modifier onlyNotOwner {
        require(msg.sender != owner);
        _;
    }

    modifier onlyAfterStart {
        require(block.timestamp >= startTime);
        _;
    }

    modifier onlyBeforeEnd {
        require(block.timestamp < endTime);
        _;
    }

    modifier onlyNotCanceled {
        require(!canceled);
        _;
    }

    modifier onlyEndedOrCanceled {
        require((block.timestamp >= endTime) || canceled);
        _;
    }
}
